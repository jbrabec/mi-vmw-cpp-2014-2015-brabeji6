/* 
 * File:   main.cpp
 * Author: brabeji
 *
 * Created on October 12, 2014, 2:37 PM
 */

using namespace std;

#include <cstdlib>
#include <iostream>
#include <chrono>

#include "opencv2/highgui/highgui.hpp"
#include "opencv2/calib3d/calib3d.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/features2d/features2d.hpp"
#include "opencv2/nonfree/nonfree.hpp"

#include <opencv2/imgproc/types_c.h>

#include <sstream>
#include <fstream>

#include <mysql_connection.h>

#include <cppconn/driver.h>
#include <cppconn/exception.h>
#include <cppconn/resultset.h>
#include <cppconn/statement.h>
#include <cppconn/prepared_statement.h>
#include <mysql_driver.h>
#include <unistd.h>

string exec(string cmd) {
	FILE* pipe = popen(cmd.c_str(), "r");
	if (!pipe) return "ERROR";
	char buffer[128];
	std::string result = "";
	while (!feof(pipe)) {
		if (fgets(buffer, 128, pipe) != NULL)
			result += buffer;
	}
	pclose(pipe);
	return result;
}

void printMat(cv::Mat m) {
	cv::FileStorage storage("mat", cv::FileStorage::MEMORY | cv::FileStorage::WRITE);
	storage << "Mat" << m;
	cout << storage.releaseAndGetString() << endl;
}

void matchDescriptors(const cv::Mat& descriptors1, const cv::Mat& descriptors2, vector<cv::DMatch> &matches, double threshold = 0.8) {

	double dist;
	double closest1dist, closest2dist;
	int closest1idx = 0, closest2idx = 0;
	for (int i = 0; i < descriptors1.rows; i++) {
		closest1dist = numeric_limits<double>::max();
		closest2dist = numeric_limits<double>::max();
		for (int j = 0; j < descriptors2.rows; j++) {
			dist = norm(descriptors1.row(i), descriptors2.row(j), cv::NORM_L2SQR);
			if (dist < closest1dist) {
				closest2dist = closest1dist;
				closest2idx = closest1idx;
				closest1dist = dist;
				closest1idx = j;
			} else if (dist < closest2dist) {
				closest2dist = dist;
				closest2idx = j;
			}
		}

		if (closest1dist / closest2dist < threshold) {
			cv::DMatch match(i, closest1idx, closest1dist);
			matches.push_back(match);
		}
	}

}

void printMatch(cv::DMatch match) {
	cout << "Image index: " << match.imgIdx
			<< ", queryIdx: " << match.queryIdx
			<< ", trainIdx: " << match.trainIdx
			<< ", distance: " << match.distance
			<< endl;
}

void crossMatchDescriptors(const cv::Mat& descriptors1, const cv::Mat& descriptors2, vector<cv::DMatch> &filteredMatches, double threshold = 0.8) {
	vector<cv::DMatch> matches12, matches21;
	matchDescriptors(descriptors1, descriptors2, matches12, threshold);
	matchDescriptors(descriptors2, descriptors1, matches21, threshold);

	for (size_t forwardIdx = 0; forwardIdx < matches12.size(); forwardIdx++) {
		//cout << "XXX: " << endl;
		//cout << "XXX: " << endl;
		//printMatch(matches12[forwardIdx]);
		//cout << "XXX: " << endl;
		for (size_t backwardIdx = 0; backwardIdx < matches21.size(); backwardIdx++) {
			//printMatch(matches21[backwardIdx]);

			if (
					matches12[forwardIdx].trainIdx == matches21[backwardIdx].queryIdx &&
					matches12[forwardIdx].queryIdx == matches21[backwardIdx].trainIdx
					) {
				filteredMatches.push_back(matches12[forwardIdx]);
				break;
			}
		}
	}

}

/*
 * 
 */
int main(int argc, char** argv) {

	// -------------------------------------------------------------------------
	// parse args	------------------------------------------------------------
	// -------------------------------------------------------------------------

	bool index = false;
	bool compare = false;
	if (!strcmp(argv[1], "--index")) {
		index = true;
	} else if (!strcmp(argv[1], "--compare")) {
		compare = true;
	} else if (!strcmp(argv[1], "--both")) {
		index = true;
		compare = true;
	} else if (!strcmp(argv[1], "--keypoints")) {
	} else {
		return -1;
	}


	string filename = argv[2];


	double MATCH_RATIO = 0.8;
	if (argc > 3) {
		istringstream MATCH_RATIOss(argv[3]);
		if (!(MATCH_RATIOss >> MATCH_RATIO)) {
			cerr << "Invalid number " << argv[3] << '\n';
			return -1;
		}
	}
	int keypointsThreshold = atoi(argv[4]);

	bool showImages = false;

	// -------------------------------------------------------------------------
	// connect db	------------------------------------------------------------
	// -------------------------------------------------------------------------
	string mysql_credentials = argv[8];
	//string mysql_credentials = "localhost:8889|root|root|recognizr";
	string mysql_host, mysql_user, mysql_pass, mysql_db;
	string token;
	int mysql_param_cnt = 0;
	while (token != mysql_credentials) {
		token = mysql_credentials.substr(0, mysql_credentials.find_first_of("|"));
		mysql_credentials = mysql_credentials.substr(mysql_credentials.find_first_of("|") + 1);
		switch (mysql_param_cnt++) {
			case 0:
				mysql_host = token;
				break;
			case 1:
				mysql_user = token;
				break;
			case 2:
				mysql_pass = token;
				break;
			case 3:
				mysql_db = token;
				break;
		}
	}
	sql::mysql::MySQL_Driver *driver;
	sql::Connection *con;
	driver = sql::mysql::get_mysql_driver_instance();
	con = driver->connect(mysql_host, mysql_user, mysql_pass);
	con->setSchema(mysql_db);

	// -------------------------------------------------------------------------
	// load image	------------------------------------------------------------
	// -------------------------------------------------------------------------
	cv::Mat img = cv::imread(filename);
	if (img.empty()) {
		cout << "File " << argv[2] << " not found." << endl;
		return -1;
	}
	//cv::cvtColor(img, img, CV_RGB2GRAY);

	string hash = exec("md5 -q " + filename);
	hash = hash.substr(0, hash.length() - 1);
	string base_path = filename.substr(0, filename.find_last_of('/'));

	// -------------------------------------------------------------------------
	// create facilities	----------------------------------------------------
	// -------------------------------------------------------------------------
	int nOctaves = atoi(argv[5]);
	int nOctaveLayers = atoi(argv[6]);
	cv::SurfDescriptorExtractor descriptorExtractor(40, nOctaves, nOctaveLayers); //, extended, upright);

	// -------------------------------------------------------------------------
	// detect keypoints	--------------------------------------------------------
	// -------------------------------------------------------------------------
	vector<cv::KeyPoint> keypoints1;

	string keypointsEngineId = argv[7];

	// measure detection time
	chrono::high_resolution_clock::time_point tBeforeDetection = chrono::high_resolution_clock::now();
	if (keypointsEngineId == "surf") {
		cv::SurfFeatureDetector detector(keypointsThreshold);
		detector.detect(img, keypoints1);
	} else {
		cv::FastFeatureDetector detector(keypointsThreshold);
		detector.detect(img, keypoints1);
		keypointsEngineId = "fast";
	}
	chrono::high_resolution_clock::time_point tAfterDetection = chrono::high_resolution_clock::now();

	cv::Mat keypointsImage;
	cv::drawKeypoints(
			img, keypoints1,
			keypointsImage,
			cv::Scalar(255, 255, 255),
			cv::DrawMatchesFlags::DRAW_RICH_KEYPOINTS);

	if (showImages) {
		cv::startWindowThread();
		cv::namedWindow("Keypoints");
		cv::namedWindow("Matches");

		cv::imshow("Keypoints", keypointsImage);
		cv::waitKey(400);
	}

	// -------------------------------------------------------------------------
	// extract features	from input image ---------------------------------------
	// -------------------------------------------------------------------------
	cv::Mat descriptors1;
	chrono::high_resolution_clock::time_point tBeforeExtraction = chrono::high_resolution_clock::now();
	descriptorExtractor.compute(img, keypoints1, descriptors1);
	chrono::high_resolution_clock::time_point tAfterExtraction = chrono::high_resolution_clock::now();

	// -------------------------------------------------------------------------
	// index image if requested	------------------------------------------------
	// -------------------------------------------------------------------------
	cout << "{";
	if (index) {
		cv::imwrite(base_path + "/" + hash + "_keypoints_" + keypointsEngineId + ".png", keypointsImage);

		cv::FileStorage descriptorsStorage("descriptors", cv::FileStorage::MEMORY | cv::FileStorage::WRITE);
		write(descriptorsStorage, "Descriptors", descriptors1);

		cv::FileStorage keypointsStorage("keypoints", cv::FileStorage::MEMORY | cv::FileStorage::WRITE);
		write(keypointsStorage, "Keypoints", keypoints1);

		sql::PreparedStatement *stmt = con->prepareStatement("INSERT INTO images (hash, filename, descriptors_" + keypointsEngineId + ", keypoints_" + keypointsEngineId + ", keypoints_threshold_" + keypointsEngineId + ", n_keypoints_" + keypointsEngineId + ", created, modified) VALUES (?, ?, ?, ?, ?, ?, NOW(), NOW()) ON DUPLICATE KEY UPDATE id=LAST_INSERT_ID(id), hash=VALUES(hash), filename=VALUES(filename), descriptors_" + keypointsEngineId + "=VALUES(descriptors_" + keypointsEngineId + "), keypoints_" + keypointsEngineId + "=VALUES(keypoints_" + keypointsEngineId + "), keypoints_threshold_" + keypointsEngineId + "=VALUES(keypoints_threshold_" + keypointsEngineId + "), n_keypoints_" + keypointsEngineId + "=VALUES(n_keypoints_" + keypointsEngineId + "), modified=VALUES(modified)");
		stmt->setString(1, hash);
		stmt->setString(2, filename);

		std::istringstream descriptorsStorageStream(descriptorsStorage.releaseAndGetString().c_str());
		stmt->setBlob(3, &descriptorsStorageStream);

		std::istringstream keypointsStorageStream(keypointsStorage.releaseAndGetString().c_str());
		stmt->setBlob(4, &keypointsStorageStream);

		stmt->setInt(5, keypointsThreshold);
		stmt->setInt(6, keypoints1.size());
		stmt->executeUpdate();
		delete stmt;
		stmt = con->prepareStatement("SELECT LAST_INSERT_ID() FROM images");
		sql::ResultSet *idRes = stmt->executeQuery();
		idRes->next();
		cout << "\"indexedId\": " << idRes->getInt(1);
		if (compare) {
			cout << ", ";
		}
		delete stmt;
	}


	// -------------------------------------------------------------------------
	// compare image if requested	--------------------------------------------
	// -------------------------------------------------------------------------
	int totalMatchingTime = 0;
	if (compare) {
		sql::PreparedStatement *pstmt = con->prepareStatement("SELECT * FROM images WHERE hash!=? AND indexed AND descriptors_" + keypointsEngineId + " IS NOT NULL");
		pstmt->setString(1, hash);
		sql::ResultSet *res = pstmt->executeQuery();


		cout << "\"matches\": {";
		while (res->next()) {
			cv::Mat savedDescriptors;
			vector<cv::KeyPoint> savedKeypoints;

			cv::FileStorage outDescriptors((string) res->getString("descriptors_" + keypointsEngineId), cv::FileStorage::READ | cv::FileStorage::MEMORY);
			read(outDescriptors["Descriptors"], savedDescriptors);
			//outDescriptors["Descriptors"] >> savedDescriptors;
			outDescriptors.release();

			cv::FileStorage outKeypoints((string) res->getString("keypoints_" + keypointsEngineId), cv::FileStorage::READ | cv::FileStorage::MEMORY);
			cv::read(outKeypoints["Keypoints"], savedKeypoints);
			//outKeypoints["Keypoints"] >> savedKeypoints;
			outKeypoints.release();

			// -----------------------------------------------------------------
			// match descriptors	--------------------------------------------
			// -----------------------------------------------------------------
			std::vector<cv::DMatch> filteredMatches;

			chrono::high_resolution_clock::time_point tBeforeMatching = chrono::high_resolution_clock::now();
			crossMatchDescriptors(descriptors1, savedDescriptors, filteredMatches, MATCH_RATIO);
			chrono::high_resolution_clock::time_point tAfterMatching = chrono::high_resolution_clock::now();
			totalMatchingTime += chrono::duration_cast<chrono::microseconds>(tAfterMatching - tBeforeMatching).count();

			int nMatches = filteredMatches.size();

			// -----------------------------------------------------------------
			// print comparison	and wait for input	----------------------------
			// -----------------------------------------------------------------
			string dbFilename = res->getString("filename");
			cv::Mat dbImage = cv::imread(dbFilename);
			//cv::cvtColor(dbImage, dbImage, CV_RGB2GRAY);

			cv::Mat matchesImage;
			cv::drawMatches(
					img, keypoints1,
					dbImage, savedKeypoints,
					filteredMatches,
					matchesImage,
					cv::Scalar(255, 255, 255), cv::Scalar(255, 255, 255), vector<char>(), cv::DrawMatchesFlags::DRAW_RICH_KEYPOINTS);
			string dbHash = res->getString("hash");
			cv::imwrite(base_path + "/" + hash + "_" + dbHash + "_" + keypointsEngineId + ".png", matchesImage);

			if (showImages) {
				cv::imshow("Matches", matchesImage);
				cv::waitKey(400);
			}

			// -----------------------------------------------------------------
			// print output	----------------------------------------------------
			// -----------------------------------------------------------------
			cout << "\"" << res->getInt("id") << "\": \""; // << ": [";
			cout << nMatches;
			cout << "\"";
			if (!res->isLast()) {
				cout << ", ";
			}
		}
		cout << "}";

		delete res;
		delete con;
	}

	// output time measurements
	cout << ",\"timing\":{";
	cout << "\"detection\":\"" << chrono::duration_cast<chrono::microseconds>(tAfterDetection - tBeforeDetection).count() << "\",";
	cout << "\"extraction\":\"" << chrono::duration_cast<chrono::microseconds>(tAfterExtraction - tBeforeExtraction).count() << "\",";
	cout << "\"matching\":\"" << totalMatchingTime << "\"";
	cout << "}";

	cout << "}";
	return 0;
}
