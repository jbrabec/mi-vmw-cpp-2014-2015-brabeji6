DROP FUNCTION IF EXISTS SPLIT_STRING;
CREATE FUNCTION SPLIT_STRING(str TEXT CHARSET utf8, delim VARCHAR(12), pos INT) RETURNS TEXT CHARSET utf8
RETURN REPLACE(SUBSTRING(SUBSTRING_INDEX(str, delim, pos),
       LENGTH(SUBSTRING_INDEX(str, delim, pos-1)) + 1),
       delim, '');

DROP PROCEDURE IF EXISTS XXX;
DELIMITER //
CREATE PROCEDURE XXX(IN a_descriptors TEXT CHARSET utf8, OUT distance TEXT CHARSET utf8)
 BEGIN
 DECLARE inp TEXT CHARSET utf8;
 DECLARE n_rows INT;
 DECLARE n_cols INT;
 DECLARE val DOUBLE;
 DECLARE descs TEXT CHARSET utf8;
 
 SELECT CONVERT(descriptors USING utf8) INTO inp FROM images LIMIT 1;

 SET n_rows = SPLIT_STRING(SPLIT_STRING(inp, '\n', 3), ':', 2);
 SET n_cols = SPLIT_STRING(SPLIT_STRING(inp, '\n', 4), ':', 2);

 SELECT SPLIT_STRING(SPLIT_STRING(inp, '[', 2), ']', 1) INTO descs;

 #SET val := TRIM(SPLIT_STRING(descs, ',', 1));

 # prepare table
 SET @table_structure := 'CREATE TEMPORARY TABLE in_descriptors (';
 SET @col_index := 0;
 create_table_loop: LOOP
  SET @col_index=@col_index+1;
  SET @table_structure := CONCAT(@table_structure, CONCAT('x', @col_index, ' DOUBLE'));
  IF @col_index=n_cols THEN
   LEAVE create_table_loop;
  ELSE
   SET @table_structure := CONCAT(@table_structure, ', ');
  END IF;
 END LOOP create_table_loop;
 SET @table_structure := CONCAT(@table_structure, ');');
 PREPARE descriptor_table_create_stmt FROM @table_structure;
 EXECUTE descriptor_table_create_stmt;

 SET @n_it := 0;

 # fill table
 SET @row_index := 0;
 SET @max_rows := n_rows;
 SET @max_cols := n_cols;
 fill_table_loop: LOOP
  SET @row_index=@row_index+1;
  SET @col_index := 0;
  SET @insert_statement := 'INSERT INTO in_descriptors VALUES (';
  fill_row_loop: LOOP
   SET @col_index=@col_index+1;
   SET @insert_statement := CONCAT(@insert_statement, '', TRIM(SPLIT_STRING(descs, ',', @row_index + @col_index)), '');
   IF @col_index=n_cols THEN
    # execute insert
    SET @insert_statement := CONCAT(@insert_statement, ');');
	#select @insert_statement;
	#SET @n_it := @n_it+1;
	#LEAVE fill_row_loop;
	#LEAVE fill_table_loop;
    PREPARE insert_statement_stmt FROM @insert_statement;
    EXECUTE insert_statement_stmt;
    LEAVE fill_row_loop;
   ELSE
    # assemble query
    SET @insert_statement := CONCAT(@insert_statement, ', ');
   END IF;
  END LOOP fill_row_loop;
  IF @row_index=n_rows THEN
   LEAVE fill_table_loop;
  END IF;
 END LOOP fill_table_loop;

 SELECT * FROM in_descriptors;

 SET distance = 'fio';

 END//
DELIMITER ;


DROP TEMPORARY TABLE IF EXISTS in_descriptors;
CALL XXX('foo', @distance);
SELECT @distance;
DESCRIBE in_descriptors;

/*
CAST(a.ar_options AS CHAR(10000) CHARACTER SET utf8)

SELECT CONVERT(descriptors USING utf8)
 FROM images
 LIMIT 1;


#SELECT SPLIT_STRING(descriptors, '\n', 9) FROM images LIMIT 1;

*/


/*
 CREATE TEMPORARY TABLE in_descriptors
 create_table_loop: LOOP
  SET a=a+1;
  SET str=SPLIT_STR(fullstr,"|",a);
  IF str='' THEN
   LEAVE create_table_loop;
  END IF;
  #Do Inserts into temp table here with str going into the row
  insert into my_temp_table values (str);

 END LOOP create_table_loop;
*/